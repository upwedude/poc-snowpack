from flask import Flask, render_template

app = Flask(__name__)

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def hello(path):
    return render_template('index.html')

if __name__ == "__main__":
    app.run("192.168.33.10", debug=True)