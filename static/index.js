"use strict";

import { createRouter } from '/static/web_modules/routerjs.js';
import mustache from '/static/web_modules/mustache.js';
import { pathShower, ajaxCall, carouselController, setupHearthStoneCards, displayCardData, cardSide } from '/static/helper/helper.js';
import store from '/static/helper/store.js';

if ("serviceWorker" in navigator) {
  navigator.serviceWorker.register("/static/sw.js", { scope: "*" }).catch((err) => {
    console.log(err);
  });
}

createRouter()
  .get('/', (req, context) => {
    pathShower('home');
    req.stop();
  })
  .get('/about', (req, context) => {
    pathShower('about');
    req.stop();
  }).run();  

setupHearthStoneCards();

async function findCards() {
  ajaxCall(true);
  const cardAttack = document.querySelector('#findCards #cardAttack').value;
  const cardHealth = document.querySelector('#findCards #cardHealth').value;

  displayCardData(false);

  try {
    const responeCard = await fetch(`https://api.magicthegathering.io/v1/cards?power=${cardAttack}&toughness=${cardHealth}`);
    const responeJson = await responeCard.json();
    const cardCarousel = document.querySelector('#cardCarousel').innerHTML;
    let cardsInSet = '';
    let hsCards = '';

    store.clearCards;
    store.cards = responeJson.cards;

    for (let current of responeJson.cards) {
      if (current.hasOwnProperty('imageUrl') && current.hasOwnProperty('name')) {
        let cardInSet = mustache.render(cardCarousel, {
          cardImage: current.imageUrl,
          cardName: current.name
        });
        cardsInSet += cardInSet;
      }
    }

    for (let current of store.hearthStoneCards) {
      if (current.attack == cardAttack && current.health == cardHealth) {
        let currentHsCard = mustache.render(cardCarousel, {
          cardImage: `https://art.hearthstonejson.com/v1/render/latest/enUS/256x/${current.id}.png`,
          cardName: current.name
        });
        hsCards += currentHsCard;
      }
    }

    document.querySelector('#cards').innerHTML = cardsInSet;
    document.querySelector('#hsCards').innerHTML = hsCards;
    
    document.querySelector('#cards > div.carousel-item').classList.add('active');
    document.querySelector('#hsCards > div.carousel-item').classList.add('active');

    document.querySelector('#responeText #mtg').innerHTML = store.filterCards;
    document.querySelector('#responeText #hs').innerHTML = store.filterHsCards;
    displayCardData(true);
  } catch (err) {
    document.querySelector('#responeText #mtg').innerHTML = `No data found for ${cardAttack}/${cardHealth}`;
    document.querySelector('#responeText #hs').innerHTML = '';
    console.log(err);
  }

  ajaxCall(false);
}

document.querySelectorAll('.carousel-control-prev-icon').forEach(current => {
  current.addEventListener('click', carouselController);
}); 
document.querySelectorAll('.carousel-control-next-icon').forEach(current => {
  current.addEventListener('click', carouselController);
}); 

document.querySelector('#home > div.btn-group.btn-group-toggle').childNodes.forEach((cur, index) => {
  cur.addEventListener('click', () => cardSide(index), false);
});

window.findCards = findCards;