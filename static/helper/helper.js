import store from '/static/helper/store.js';

export function pathShower(elementId) {
  document.querySelectorAll('#app article').forEach((current) => {
    current.style.display = 'none';
    if (current.id == elementId) {
      current.style.display = 'block';
    }
  });
}

export function ajaxCall(action) {
  if (action) {
    document.querySelector('#backgroundAjax').style.display = 'block';
  } else {
    document.querySelector('#backgroundAjax').style.display = 'none';  
  }
}

export function carouselController(event) {
  const carouselId = event.target.parentElement.parentElement.id;
  let currentActive = document.querySelector(`#${carouselId} .active`);
  let cardType = '';

  currentActive.classList.remove('active');

  if (event.target.classList[0].includes('next')) {
    if (currentActive.nextElementSibling != null) {
      currentActive.nextElementSibling.classList.add('active');
    } else {
      document.querySelectorAll(`#${carouselId} .carousel-item`)[0].classList.add('active');
    }
  } else {
    if (currentActive.previousElementSibling != null) {
      currentActive.previousElementSibling.classList.add('active');
    } else {
      const tmp = document.querySelectorAll(`#${carouselId} .carousel-item`).length -1;
      document.querySelectorAll(`#${carouselId} .carousel-item`)[tmp].classList.add('active');
    }
  }
  if (carouselId == 'carouselHsControls') {
    cardType = 'hs';
    document.querySelector(`#responeText #${cardType}`).innerHTML = store.filterHsCards;
  } else {
    cardType = 'mtg';
    document.querySelector(`#responeText #${cardType}`).innerHTML = store.filterCards;
  } 
}

export async function setupHearthStoneCards() {
  const responeCard = await fetch('https://api.hearthstonejson.com/v1/25770/enUS/cards.collectible.json');
  const responeJson = await responeCard.json();
  store.hearthStoneCards = responeJson;
}

export function displayCardData(state) {
  const cardsButon = document.querySelector('#home > div.btn-group.btn-group-toggle');

  if (state) {
    document.querySelectorAll('.carousel-control-next-icon, .carousel-control-prev-icon').forEach(cur => {
      cur.style.display = 'block';
    });

    cardsButon.children[1].classList.remove('btn-secondary');
    cardsButon.children[1].classList.add('btn-success');
  } else {
    document.querySelectorAll('.carousel-control-next-icon, .carousel-control-prev-icon').forEach(cur => {
      cur.style.display = 'none';
    });

    cardsButon.children[1].classList.add('btn-secondary');
    cardsButon.children[1].classList.remove('btn-success');
  }
}

export async function cardSide(stateInt) {
  const state = stateInt == 1 ? true : false;
  const cardBody = document.querySelector(!state ? '#slideActive' : '#backSide');

  if (store.cards.length >= 1 && store.hearthStoneCards.length >= 1 ) {
    cardBody.style.animation = "";
    cardBody.style.animation = "cardFlip 1.5s infinite";
    await new Promise(r => setTimeout(r, 1400));
    cardBody.style.animation = "";
    cardFace(state)
  } else {
    cardBody.style.animation = "";
  }
}

async function cardFace(state) {
  if (state) {
    document.querySelector('#backSide').style.display = 'none';
    document.querySelector('#slideActive').style.display = 'flex';
  } else {
    const chartist = await import('/static/web_modules/chartist.js');
    const mtgData = {
      labels: getCardData(false, true),
      series: [
        getCardData(true, true),
      ]
    };
    const hsData = {
      labels: getCardData(false, false),
      series: [
        getCardData(true, false),
      ]
    };
    new chartist.default.Bar('#mtgChart', mtgData);
    new chartist.default.Bar('#hsChart', hsData);

    document.querySelector('#backSide').style.display = 'flex';
    document.querySelector('#slideActive').style.display = 'none';
  }
}

function getCardData(state, game) {
  let returnArray = [];

  if (game) {
    if (state) {
      store.cards.forEach((cur) => {
        returnArray.push(cur.cmc)
      });
    } else {
      store.cards.forEach((cur) => {
        returnArray.push(cur.name)
      });
    }
  } else {
    if (state) {
      store.filterHsByCost.forEach((cur) => {
        returnArray.push(cur.cost)
      });
    } else {
      store.filterHsByCost.forEach((cur) => {
        returnArray.push(cur.name)
      });
    }
  }

  return returnArray;
}