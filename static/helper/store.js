import { observable } from '/static/web_modules/mobx.js'; 

let store = observable({
  cards: [],
  get filterCards() {
    const currentCardImg = document.querySelector('#carouselControls .active > img');
    for (let tmp of this.cards) {
      if (currentCardImg.alt == tmp.name) {
        if (tmp.subtypes.length == 0) {
          return `${tmp.name} is ${tmp.types}`;
        } else if (typeof tmp.power == 'undefined') {
          return `${tmp.name} is ${tmp.types} - ${tmp.subtypes}`;
        } else {
          return `${tmp.name} is ${tmp.types} - ${tmp.subtypes} with ${tmp.power}/${tmp.toughness} stats`;
        }
      }
    }
  },
  get filterHsCards() {
    const currentCardImg = document.querySelector('#carouselHsControls .active > img');

    for (let tmp of this.hearthStoneCards) {
      if (currentCardImg.alt == tmp.name) {
        if (typeof tmp.race == 'undefined') {
          return `${tmp.name} is ${tmp.type} with ${tmp.attack}/${tmp.health} stats`;
        } else {
          return `${tmp.name} is ${tmp.type} - ${tmp.race} with ${tmp.attack}/${tmp.health} stats`;
        }
      }
    }
  },
  get filterHsByCost() {
    const cardAttack = document.querySelector('#findCards #cardAttack').value;
    const cardHealth = document.querySelector('#findCards #cardHealth').value;
    let returnCards = [];

    for (let tmp of this.hearthStoneCards) {
      if (tmp.attack == cardAttack && tmp.health == cardHealth) {
        returnCards.push(tmp);
      }
    }
    return returnCards;
  },
  clearCards() {
    this.cards = []
  },
  hearthStoneCards: []
});

export default store