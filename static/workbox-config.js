module.exports = {
  "globDirectory": "/vagrant/static",
  "globPatterns": [
    "**/*.{ttf,js,png,ico,css}"
  ],
  "swDest": "/vagrant/static/sw.js"
};